package fr.ulille.iut.todo.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import fr.ulille.iut.todo.service.Tache;

/**
 * TacheDAO
 */
public interface TacheDAO {
    @SqlUpdate("create table if not exists taches (id varchar(128) primary key, nom varchar not null, description varchar)")
    void createTable();

    @SqlUpdate("drop table if exists taches")
    void dropTable();

    @SqlUpdate("insert into taches (id, nom, description) values (:id, :nom, :description)")
    void insert(@BindBean Tache tache);

    @SqlUpdate("update taches set nom = :nom, description = :description where id = :id")
    void update(@BindBean Tache tache);

    @SqlUpdate("delete from taches where id = ?")
    int delete(String id);

    @SqlQuery("select * from taches where id = ?") 
    @RegisterBeanMapper(Tache.class)
    Tache getById(String id);

    @SqlQuery("select * from taches")
    @RegisterBeanMapper(Tache.class)
    List<Tache> getAll();
}

