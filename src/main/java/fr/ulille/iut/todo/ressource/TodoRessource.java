package fr.ulille.iut.todo.ressource;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import fr.ulille.iut.todo.dto.CreationTacheDTO;
import fr.ulille.iut.todo.service.Tache;
import fr.ulille.iut.todo.service.TodoService;
import jakarta.annotation.security.PermitAll;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.EntityTag;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.UriInfo;

@Path("taches")
@Produces(MediaType.APPLICATION_JSON)
@PermitAll
public class TodoRessource {
    private final static Logger LOGGER = Logger.getLogger(TodoRessource.class.getName());

    private TodoService todoService = new TodoService();

    @Context
    private UriInfo uri;
    @Context
    Request request;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<Tache> getAll() {
        LOGGER.info("getAll()");

        return todoService.getAll();
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getTache(@PathParam("id") String id) {
        LOGGER.info("getTache()");

        Tache tache = todoService.getTache(UUID.fromString(id));
        if (tache == null) {
            throw new WebApplicationException("La tâche n'existe pas : " + id, Response.Status.NOT_FOUND);
        }

        EntityTag etag = new EntityTag(Integer.toString(tache.hashCode()));
        ResponseBuilder builder = request.evaluatePreconditions(etag);

        if (builder == null) {
            builder = Response.ok();
            builder.tag(etag);
            builder.entity(tache);
        }
        return builder.build();
    }

    @GET
    @Path("{id}/description")
    public String getDescription(@PathParam("id") String id) {
        LOGGER.info("getDescription()");

        Tache tache = todoService.getTache(UUID.fromString(id));
        if (tache == null) {
            throw new WebApplicationException("La tâche n'existe pas : " + id, Response.Status.NOT_FOUND);
        }
        return tache.getDescription();
    }

    @POST
    public Response createTache(CreationTacheDTO tacheDto) {
        LOGGER.info("createTache()");

        Tache tache = Tache.fromCreationTacheDTO(tacheDto);
        todoService.addTache(tache);
        URI location = uri.getAbsolutePathBuilder().path(tache.getId().toString()).build();

        EntityTag etag = new EntityTag(Integer.toString(tache.hashCode()));
        ResponseBuilder builder = request.evaluatePreconditions(etag);

        if (builder == null) {
            builder = Response.created(location);
            builder.tag(etag);
            builder.entity(tache);
        }
        return builder.build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response createTacheFromForm(MultivaluedMap<String, String> formParams) {
        Tache tache = new Tache();
        tache.setNom(formParams.getFirst("nom"));
        tache.setDescription(formParams.getFirst("description"));

        todoService.addTache(tache);
        URI location = uri.getAbsolutePathBuilder().path(tache.getId().toString()).build();

        EntityTag etag = new EntityTag(Integer.toString(tache.hashCode()));
        ResponseBuilder builder = request.evaluatePreconditions(etag);

        if (builder == null) {
            builder = Response.created(location);
            builder.tag(etag);
            builder.entity(tache);
        }
        return builder.build();
    }

    @DELETE
    @Path("{id}")
    public Response deleteTache(@PathParam("id") String id) {
        LOGGER.info("deleteTache()");

        if (todoService.deleteTache(id) == 0) {
            throw new WebApplicationException("La tâche n'existe pas : " + id, Response.Status.NOT_FOUND);
        } else
            return Response.noContent().build();
    }

    @PUT
    @Path("{id}")
    public Response updateTache(@PathParam("id") String id, CreationTacheDTO dto) {
        LOGGER.info("updateTache");

        Tache tache = todoService.getTache(UUID.fromString(id));
        if (tache == null) {
            throw new WebApplicationException("La tâche n'existe pas : " + id, Response.Status.NOT_FOUND);
        }
        if (dto.getNom() == null || dto.getDescription() == null) {
            throw new WebApplicationException("Format incorrect : " + id, Response.Status.NOT_ACCEPTABLE);
        }
        tache.setNom(dto.getNom());
        tache.setDescription(dto.getDescription());

        todoService.updateTache(tache);

        EntityTag etag = new EntityTag(Integer.toString(tache.hashCode()));
        ResponseBuilder builder = request.evaluatePreconditions(etag);

        if (builder == null) {
            builder = Response.accepted();
            builder.tag(etag);
            builder.entity(tache);
        }
        return builder.build();
    }
}
