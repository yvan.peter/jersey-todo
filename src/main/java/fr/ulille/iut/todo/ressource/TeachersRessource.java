package fr.ulille.iut.todo.ressource;

import java.util.Arrays;
import java.util.List;

import jakarta.annotation.security.RolesAllowed;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("teachers")
@Produces(MediaType.APPLICATION_JSON)
public class TeachersRessource {
    @RolesAllowed("ADMIN")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> getAllTeachers() {
        return Arrays.asList(new String[] {"Yvan Peter", "Jean-Marie Place", "Frédéric Guyomarch"});
    }
}   