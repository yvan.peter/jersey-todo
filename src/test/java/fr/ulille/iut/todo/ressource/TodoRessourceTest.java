package fr.ulille.iut.todo.ressource;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.todo.BDDFactory;
import fr.ulille.iut.todo.DebugMapper;
import fr.ulille.iut.todo.dao.TacheDAO;
import fr.ulille.iut.todo.dto.CreationTacheDTO;
import fr.ulille.iut.todo.service.Tache;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class TodoRessourceTest extends JerseyTest {
    private TacheDAO dao;

    @Override
    protected Application configure() {
        BDDFactory.setJdbiForTests();
        ResourceConfig rc = new ResourceConfig(TodoRessource.class);
        // Dé-commenter pour avoir la trace des requêtes et réponses
        //rc.register(new LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME), Level.INFO,
        //        LoggingFeature.Verbosity.PAYLOAD_ANY, 10000));
        // Dé-commenter pour avoir une trace des erreurs internes serveur (les tests sur le code de retour vont échouer)
        //rc.register(DebugMapper.class);

        return rc;
    }

    @Before
    public void setEnvUp() {
        dao = BDDFactory.buildDao(TacheDAO.class);
        dao.createTable();
    }

    @After
    public void tearEnvDown() throws Exception {
       dao.dropTable();
    }

    @Test
    public void get_should_return_empty_list_at_startup() {
        List<Tache> answer = target("taches").request(MediaType.APPLICATION_JSON).get(new GenericType<List<Tache>>(){});
        assertEquals(new ArrayList<Tache>(), answer);
    }

    @Test
    public void post_should_return_201_and_task() {
        CreationTacheDTO dto = new CreationTacheDTO();
        dto.setNom("test");
        dto.setDescription("description test");

        Response response = target("taches").request().post(Entity.json(dto));
        assertEquals(201, response.getStatus());

        Tache tache = response.readEntity(Tache.class);
        assertEquals(dto.getNom(), tache.getNom());
    }

    @Test
    public void get_with_id_should_return_task() {
        Tache tache = new Tache();
        tache.setNom("test");
        tache.setDescription("description");

        dao.insert(tache);
        Tache returned= target("taches").path(tache.getId().toString()).request().get(Tache.class);

        assertEquals(tache, returned);
    }

    @Test
    public void get_for_description_should_work() {
        Tache tache = new Tache();
        tache.setNom("test");
        tache.setDescription("got description");

        dao.insert(tache);

        String description = target("taches").path(tache.getId().toString()).path("description").request().get(String.class);

        assertEquals("got description", description);
    }

    @Test
    public void delete_should_remove_task() {
        Tache tache = new Tache();
        tache.setNom("test");
        tache.setDescription("to delete");

        dao.insert(tache);

        Response response = target("taches").path(tache.getId().toString()).request().delete();

        assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response.getStatus());
    }

    @Test
    public void put_should_replace_existing_task_value() {
        Tache tache = new Tache();
        tache.setNom("test");
        tache.setDescription("to delete");

        dao.insert(tache);

        CreationTacheDTO updateTask = new CreationTacheDTO();
        updateTask.setNom("new name");
        updateTask.setDescription("new Description");

        Response response = target("taches").path(tache.getId().toString()).request().put(Entity.json(updateTask));

        assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

        Tache result = response.readEntity(Tache.class);
        assertEquals(updateTask.getNom(), result.getNom());
    }

    @Test
    public void partial_put_should_fail_with_406() {
         Tache tache = new Tache();
        tache.setNom("test");
        tache.setDescription("to delete");

        dao.insert(tache);

        CreationTacheDTO updateTask = new CreationTacheDTO();
        updateTask.setNom("new name");

        Response response = target("taches").path(tache.getId().toString()).request().put(Entity.json(updateTask));

        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
    }

    @Test
    public void post_with_form_data_should_work() {
        Form form = new Form();
        form.param("nom", "tacheForm");
        form.param("description", "description form");

        Response response = target("taches").request().post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED));
        assertEquals(201, response.getStatus());

        Tache tache = response.readEntity(Tache.class);
        assertEquals("tacheForm", tache.getNom());
    }
}
