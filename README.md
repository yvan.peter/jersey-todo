# Développement d'une ressource ToDO pour le TP HTTP de M4102

## Génération du code initial
1. Utilisation de l'archétype Maven de Jersey pour la génération du code :

   mvn archetype:generate -DarchetypeArtifactId=jersey-quickstart-grizzly2 -DarchetypeGroupId=org.glassfish.jersey.archetypes -DinteractiveMode=false -DgroupId=fr.ulille.iut -DartifactId=jersey-todo -Dpackage=fr.ulille.iut.todo
   
1. Passage en JDK 11 (et à un plugin de compilation plus récent)
```xml
       <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <inherited>true</inherited>
                <configuration>
                  <release>11</release>
                </configuration>
            </plugin>
```

1. Activation du support JSON
```xml
        <!-- uncomment this to get JSON support: -->
         <dependency>
          <groupId>org.glassfish.jersey.media</groupId>
            <artifactId>jersey-media-json-binding</artifactId>
        </dependency>
        <!-- -->
```

1. Ajout de `Maven shade` pour la création d'un uberJar exécutable

# Ajout des logs des requêtes
```java
    public class Main {
      public static HttpServer startServer() {
        ...
        ResourceConfig rc = new ResourceConfig().packages("fr.ulille.iut.todo");
        // Activation des log des requêtes et réponses
        rc.register(new LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME), Level.INFO,
                LoggingFeature.Verbosity.PAYLOAD_ANY, 10000));
```

# Développement de la ressource

# Authentification

## Basic auth avec Base64
La liste des enseignants (```@Path teachers```) nécessite une authentification. Seul l'utilisateur ```root``` peut y accéder avec le mot de passe ```m4102```. 

On peut passer l'utilisateur à ```curl```avec l'option ```-u root``` puis saisir le mot de passe interactivement dans le shell.
```sh
curl -u root http://localhost:8080/api/v1/teachers
```

Autre solution, on donne l'entête complète avec l'option ```-H```et en encodant les infomations de login avec le script ```base64```
```
curl -H "Authorization: Basic $(echo -n root:m4102 | base64)" http://localhost:8080/api/v1/teachers
```
